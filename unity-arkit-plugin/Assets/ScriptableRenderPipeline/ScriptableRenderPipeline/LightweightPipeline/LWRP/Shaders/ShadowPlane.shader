Shader "Custom/Shadow Plane"
{
    Properties
    {
        _Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5
        
        _Color("Shadow Color", Color) = (0.1, 0.1, 0.1)
        _Opacity("Shadow Opacity", Float) = 0.3
        
        _ShadowDistance("ShadowDistance", Float) = 8.0
    }

    SubShader
    {

        Tags{"RenderType" = "Transparent" "Queue" = "Transparent" "IgnoreProjector" = "True"}
        LOD 300

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite On
            Cull Back
            //AlphaTest Less 0.5

            HLSLPROGRAM
            // Required to compile gles 2.0 with standard SRP library
            // All shaders must be compiled with HLSLcc and currently only gles is not using HLSLcc by default
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0
            #define _SHADOWS_SOFT

            #pragma vertex LitPassVertex
            #pragma fragment LitPassFragment
            #include "LWRP/ShaderLibrary/Lighting.hlsl"
            
            struct LightweightVertexInput
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };
            
            struct LightweightVertexOutput
            {
                float3 posWS                    : TEXCOORD2;
                float4 posVS                    : TEXCOORD3;
                float4 shadowCoord              : TEXCOORD7;
                float4 clipPos                  : SV_POSITION;
                
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };
            
            LightweightVertexOutput LitPassVertex(LightweightVertexInput v)
{
                LightweightVertexOutput o = (LightweightVertexOutput)0;
            
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o);

                float3 posWS = TransformObjectToWorld(v.vertex.xyz);
                o.clipPos = TransformWorldToHClip(posWS);

                o.shadowCoord = TransformWorldToShadowCoord(posWS);
                o.posWS = posWS;
                o.posVS = mul(UNITY_MATRIX_V, float4(posWS, 1.0));
            
                return o;
            }
            
            float _Opacity;
            float4 _Color;
            float _ShadowDistance;
            
            // Used in Standard (Physically Based) shader
            half4 LitPassFragment(LightweightVertexOutput IN) : SV_Target
            {
   
                float d = 0.0;
                
                d = clamp(length(IN.posVS.z)/8.0, 0.0, 1.0);
                half4 color = half4(_Color.r, _Color.g, _Color.b, 0.0);
                
                if(d >= 1.0)
                    discard;
                
                float attenuation =( 1.0 - SampleShadowmap(IN.shadowCoord)) * _Opacity;
                
                color.a = attenuation * (1.0 - pow(d, 3.0));

                return color;
            }
            
            ENDHLSL
        }

    }
}