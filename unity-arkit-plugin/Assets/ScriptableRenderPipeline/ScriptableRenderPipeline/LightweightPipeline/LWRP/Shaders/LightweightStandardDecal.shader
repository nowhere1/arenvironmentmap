Shader "LightweightPipeline/Decal (Physically Based)"
{
    Properties
    {
        // Specular vs Metallic workflow
        [HideInInspector] _WorkflowMode("WorkflowMode", Float) = 1.0

        _Color("Color", Color) = (0.5,0.5,0.5,1)
        _MainTex("Albedo", 2D) = "white" {}

        _Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

        _Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5
        _GlossMapScale("Smoothness Scale", Range(0.0, 1.0)) = 1.0
        _SmoothnessTextureChannel("Smoothness texture channel", Float) = 0
        
        _DepthInput("DepthInput", 2D) = "white" {}

        [Gamma] _Metallic("Metallic", Range(0.0, 1.0)) = 0.0
        _MetallicGlossMap("Metallic", 2D) = "white" {}

        _SpecColor("Specular", Color) = (0.2, 0.2, 0.2)
        _SpecGlossMap("Specular", 2D) = "white" {}
        
        _DecalPos("DecalPos", Vector) = (0.0, 0.0, 0.0, 1.0)

        [ToggleOff] _SpecularHighlights("Specular Highlights", Float) = 1.0
        [ToggleOff] _GlossyReflections("Glossy Reflections", Float) = 1.0

        _BumpScale("Scale", Float) = 1.0
        _BumpMap("Normal Map", 2D) = "bump" {}

        _OcclusionStrength("Strength", Range(0.0, 1.0)) = 1.0
        _OcclusionMap("Occlusion", 2D) = "white" {}

        _EmissionColor("Color", Color) = (0,0,0)
        _EmissionMap("Emission", 2D) = "white" {}

        [Enum(UV0,0,UV1,1)] _UVSec("UV Set for secondary textures", Float) = 0

        // Blending state
        [HideInInspector] _Surface("__surface", Float) = 0.0
        [HideInInspector] _Blend("__blend", Float) = 0.0
        [HideInInspector] _AlphaClip("__clip", Float) = 0.0
        [HideInInspector] _SrcBlend("__src", Float) = 1.0
        [HideInInspector] _DstBlend("__dst", Float) = 0.0
        [HideInInspector] _ZWrite("__zw", Float) = 1.0
        [HideInInspector] _Cull("__cull", Float) = 2.0
    }

    SubShader
    {
        // Lightweight Pipeline tag is required. If Lightweight pipeline is not set in the graphics settings
        // this Subshader will fail. One can add a subshader below or fallback to Standard built-in to make this
        // material work with both Lightweight Pipeline and Builtin Unity Pipeline
        Tags{"RenderType" = "Opaque" "Queue"="Transparent" "RenderPipeline" = "LightweightPipeline" "IgnoreProjector" = "True"}
        LOD 300

        // ------------------------------------------------------------------
        //  Forward pass. Shades all light in a single pass. GI + emission + Fog
        Pass
        {
            // Lightmode matches the ShaderPassName set in LightweightPipeline.cs. SRPDefaultUnlit and passes with
            // no LightMode tag are also rendered by Lightweight Pipeline
            Tags{"LightMode" = "LightweightForward"}

            Blend[_SrcBlend][_DstBlend]
            ZTest GEqual
            ZWrite Off
            Cull Front

            HLSLPROGRAM
            // Required to compile gles 2.0 with standard SRP library
            // All shaders must be compiled with HLSLcc and currently only gles is not using HLSLcc by default
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0

            // -------------------------------------
            // Material Keywords
            #pragma shader_feature _NORMALMAP
            #pragma shader_feature _ALPHATEST_ON
            #pragma shader_feature _ALPHAPREMULTIPLY_ON
            #pragma shader_feature _EMISSION
            #pragma shader_feature _METALLICSPECGLOSSMAP
           // #pragma shader_feature _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
            #pragma shader_feature _OCCLUSIONMAP

           // #pragma shader_feature _SPECULARHIGHLIGHTS_OFF
          //  #pragma shader_feature _GLOSSYREFLECTIONS_OFF
            #pragma shader_feature _SPECULAR_SETUP

            // -------------------------------------
            // Lightweight Pipeline keywords
            #pragma multi_compile _ _ADDITIONAL_LIGHTS
            //#pragma multi_compile _ _VERTEX_LIGHTS
            #pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE
            //#pragma multi_compile _ FOG_LINEAR FOG_EXP2
            #pragma multi_compile _ _SHADOWS_ENABLED

            // -------------------------------------
            // Unity defined keywords
            //#pragma multi_compile _ DIRLIGHTMAP_COMBINED
            //#pragma multi_compile _ LIGHTMAP_ON

            #pragma vertex LitPassVertex
            #pragma fragment Frag
            
            #define NO_SHADOWS

            #include "LWRP/ShaderLibrary/InputSurfacePBR.hlsl"
            #include "LWRP/ShaderLibrary/LightweightPassDecalLit.hlsl"

            sampler2D _DepthInput;
            
             float4x4 inverse(float4x4 input)
             {
                 #define minor(a,b,c) determinant(float3x3(input.a, input.b, input.c))
                 //determinant(float3x3(input._22_23_23, input._32_33_34, input._42_43_44))
                 
                 float4x4 cofactors = float4x4(
                      minor(_22_23_24, _32_33_34, _42_43_44), 
                     -minor(_21_23_24, _31_33_34, _41_43_44),
                      minor(_21_22_24, _31_32_34, _41_42_44),
                     -minor(_21_22_23, _31_32_33, _41_42_43),
                     
                     -minor(_12_13_14, _32_33_34, _42_43_44),
                      minor(_11_13_14, _31_33_34, _41_43_44),
                     -minor(_11_12_14, _31_32_34, _41_42_44),
                      minor(_11_12_13, _31_32_33, _41_42_43),
                     
                      minor(_12_13_14, _22_23_24, _42_43_44),
                     -minor(_11_13_14, _21_23_24, _41_43_44),
                      minor(_11_12_14, _21_22_24, _41_42_44),
                     -minor(_11_12_13, _21_22_23, _41_42_43),
                     
                     -minor(_12_13_14, _22_23_24, _32_33_34),
                      minor(_11_13_14, _21_23_24, _31_33_34),
                     -minor(_11_12_14, _21_22_24, _31_32_34),
                      minor(_11_12_13, _21_22_23, _31_32_33)
                 );
                 #undef minor
                 return transpose(cofactors) / determinant(input);
             }
             
            float4 _DecalPos;
            float4 _DecalNormalDir;
            float4x4 _ViewProj;
            
            float4x4 camera_ViewInverse;
            float4x4 camera_ViewProjInverse;
            
            half4 Frag(LightweightVertexOutput IN) : SV_Target
            {
                float4 baseColor = float4(0, 0, 0, 1);
                
                
                float2 uvCoord = (IN.clipPosCopy.xy/IN.clipPosCopy.w + 1) * 0.5;
  
                float depth = tex2D(_DepthInput, float2(uvCoord.x, 1.0 - uvCoord.y)).r;
                
                float4 H = float4((uvCoord.x) * 2 - 1, (uvCoord.y) * 2 - 1, depth, 1.0);
                float4 D = mul(inverse(UNITY_MATRIX_VP), H);
                
                float4 wPos = D/D.w;
                float4 vPos = mul(UNITY_MATRIX_V, wPos);
                
                float4 decalSpacePos = mul(_ViewProj, float4(wPos.x, wPos.y, wPos.z, 1));

                float4 normalWS = mul(
                                      camera_ViewInverse, 
                                      float4(cross(normalize(ddx(vPos.xyz)), normalize(ddy(vPos.xyz))), 1.0)
                                      );
                //normalWS.xy *= normalWS.z;
                //normalWS.z = -normalWS.z;
                normalWS = normalize(normalWS);
           
                float2 decalUV = clamp((decalSpacePos.xy + 1) * 0.5, 0, 1);
                
                Light mainLight = GetMainLight(wPos);
                
                SurfaceData surfaceData;
                InitializeStandardLitSurfaceData(IN.uv, surfaceData);

                InputData inputData;
                InitializeInputData(IN, normalWS, wPos.xyz, inputData);
   

                //light
                float4 lightTerm = max(float4( mainLight.color * mainLight.attenuation * saturate(dot(normalize(mainLight.direction), normalWS)), 1.0), 0.3);
                
                //albedo
                baseColor = SampleAlbedoAlpha(decalUV, TEXTURE2D_PARAM(_MainTex, sampler_MainTex));

                float amount = abs(decalSpacePos.z);
                amount = smoothstep(1.0, 0.6, amount);
                
                //baseColor.rgb = mul(UNITY_MATRIX_V, normalWS.rgb);
                baseColor.a *= amount;
                //baseColor.a = 1;
                
                baseColor = LightweightFragmentPBR(inputData, baseColor.rgb, surfaceData.metallic, surfaceData.specular, surfaceData.smoothness, 1, surfaceData.emission, baseColor.a);
   
                return baseColor;
            }
            ENDHLSL
        }

    }
    FallBack "Hidden/InternalErrorShader"
    CustomEditor "LightweightStandardGUI"
}
