Shader "LightweightPipeline/StandardHands (Physically Based)"
{
    Properties
    {
        // Specular vs Metallic workflow
        [HideInInspector] _WorkflowMode("WorkflowMode", Float) = 1.0

        _Color("Color", Color) = (0.5,0.5,0.5,1)
        _MainTex("Albedo", 2D) = "white" {}

        _Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

        _Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5
        _GlossMapScale("Smoothness Scale", Range(0.0, 1.0)) = 1.0
        _SmoothnessTextureChannel("Smoothness texture channel", Float) = 0
        
        _DepthInput("DepthInput", 2D) = "white" {}

        [Gamma] _Metallic("Metallic", Range(0.0, 1.0)) = 0.0
        _MetallicGlossMap("Metallic", 2D) = "white" {}

        _SpecColor("Specular", Color) = (0.2, 0.2, 0.2)
        _SpecGlossMap("Specular", 2D) = "white" {}

        [ToggleOff] _SpecularHighlights("Specular Highlights", Float) = 1.0
        [ToggleOff] _GlossyReflections("Glossy Reflections", Float) = 1.0

        _BumpScale("Scale", Float) = 1.0
        _BumpMap("Normal Map", 2D) = "bump" {}

        _OcclusionStrength("Strength", Range(0.0, 1.0)) = 0.0
        _OcclusionMap("Occlusion", 2D) = "white" {}

        _EmissionColor("Color", Color) = (0,0,0)
        _EmissionMap("Emission", 2D) = "white" {}
        
        _textureY ("TextureY", 2D) = "white" {}
        _textureCbCr ("TextureCbCr", 2D) = "black" {}
        
        _Pattern1 ("Pattern1", 2D) = "black" {}
        _Pattern2 ("Pattern2", 2D) = "black" {}
        
        _StealthAmount("StealthAmount", Range(0.0, 1.0)) = 0.0

        [Enum(UV0,0,UV1,1)] _UVSec("UV Set for secondary textures", Float) = 0

        // Blending state
        [HideInInspector] _Surface("__surface", Float) = 0.0
        [HideInInspector] _Blend("__blend", Float) = 0.0
        [HideInInspector] _AlphaClip("__clip", Float) = 0.0
        [HideInInspector] _SrcBlend("__src", Float) = 1.0
        [HideInInspector] _DstBlend("__dst", Float) = 0.0
        [HideInInspector] _ZWrite("__zw", Float) = 1.0
        [HideInInspector] _Cull("__cull", Float) = 2.0
    }

    SubShader
    {
        // Lightweight Pipeline tag is required. If Lightweight pipeline is not set in the graphics settings
        // this Subshader will fail. One can add a subshader below or fallback to Standard built-in to make this
        // material work with both Lightweight Pipeline and Builtin Unity Pipeline
        Tags{"RenderType" = "Opaque" "Queue"="Geometry" "RenderPipeline" = "LightweightPipeline" "IgnoreProjector" = "True"}
        LOD 300

        // ------------------------------------------------------------------
        //  Forward pass. Shades all light in a single pass. GI + emission + Fog
        Pass
        {
            // Lightmode matches the ShaderPassName set in LightweightPipeline.cs. SRPDefaultUnlit and passes with
            // no LightMode tag are also rendered by Lightweight Pipeline
            Tags{"LightMode" = "LightweightForward"}

            Blend[_SrcBlend][_DstBlend]
            ZTest LEqual
            ZWrite On
            Cull Back

            HLSLPROGRAM
            // Required to compile gles 2.0 with standard SRP library
            // All shaders must be compiled with HLSLcc and currently only gles is not using HLSLcc by default
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0

            // -------------------------------------
            // Material Keywords
            #pragma shader_feature _NORMALMAP
            #pragma shader_feature _ALPHATEST_ON
            #pragma shader_feature _ALPHAPREMULTIPLY_ON
            #pragma shader_feature _EMISSION
            #pragma shader_feature _METALLICSPECGLOSSMAP
           // #pragma shader_feature _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
            #pragma shader_feature _OCCLUSIONMAP

            //#pragma shader_feature _SPECULARHIGHLIGHTS_OFF
            //#pragma shader_feature _GLOSSYREFLECTIONS_OFF
            #pragma shader_feature _SPECULAR_SETUP

            // -------------------------------------
            // Lightweight Pipeline keywords
            #pragma multi_compile _ _ADDITIONAL_LIGHTS
            //#pragma multi_compile _ _VERTEX_LIGHTS
            #pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE
            //#pragma multi_compile _ FOG_LINEAR FOG_EXP2
            #pragma multi_compile _ _SHADOWS_ENABLED

            // -------------------------------------
            // Unity defined keywords
            //#pragma multi_compile _ DIRLIGHTMAP_COMBINED
            //#pragma multi_compile _ LIGHTMAP_ON

            #pragma vertex LitPassVertex
            #pragma fragment Frag
            
            #define NO_SHADOWS

            #include "LWRP/ShaderLibrary/InputSurfacePBR.hlsl"
            #include "LWRP/ShaderLibrary/LightweightPassDecalLit.hlsl"

            sampler2D _DepthInput;
            
             float4x4 inverse(float4x4 input)
             {
                 #define minor(a,b,c) determinant(float3x3(input.a, input.b, input.c))
                 //determinant(float3x3(input._22_23_23, input._32_33_34, input._42_43_44))
                 
                 float4x4 cofactors = float4x4(
                      minor(_22_23_24, _32_33_34, _42_43_44), 
                     -minor(_21_23_24, _31_33_34, _41_43_44),
                      minor(_21_22_24, _31_32_34, _41_42_44),
                     -minor(_21_22_23, _31_32_33, _41_42_43),
                     
                     -minor(_12_13_14, _32_33_34, _42_43_44),
                      minor(_11_13_14, _31_33_34, _41_43_44),
                     -minor(_11_12_14, _31_32_34, _41_42_44),
                      minor(_11_12_13, _31_32_33, _41_42_43),
                     
                      minor(_12_13_14, _22_23_24, _42_43_44),
                     -minor(_11_13_14, _21_23_24, _41_43_44),
                      minor(_11_12_14, _21_22_24, _41_42_44),
                     -minor(_11_12_13, _21_22_23, _41_42_43),
                     
                     -minor(_12_13_14, _22_23_24, _32_33_34),
                      minor(_11_13_14, _21_23_24, _31_33_34),
                     -minor(_11_12_14, _21_22_24, _31_32_34),
                      minor(_11_12_13, _21_22_23, _31_32_33)
                 );
                 #undef minor
                 return transpose(cofactors) / determinant(input);
             }
  
            float4 _DecalPos;
            float4 _DecalNormalDir;
            float4x4 _ViewProj;
            
            float4x4 camera_ViewInverse;
            float4x4 camera_ViewProjInverse;
            
            sampler2D _textureY;
            sampler2D _textureCbCr;
            
            sampler2D _Pattern1;
            sampler2D _Pattern2;
            
            float _StealthAmount;
            
            void InitializeInputData(LightweightVertexOutput IN, half3 normalTS, out InputData inputData)
            {
                inputData = (InputData)0;
            
                inputData.positionWS = IN.posWS;
            
            #ifdef _NORMALMAP
                half3 viewDir = half3(IN.normal.w, IN.tangent.w, IN.binormal.w);
                inputData.normalWS = TangentToWorldNormal(normalTS, IN.tangent.xyz, IN.binormal.xyz, IN.normal.xyz);
            #else
                half3 viewDir = IN.viewDir;
                inputData.normalWS = FragmentNormalWS(IN.normal);
            #endif
            
                inputData.viewDirectionWS = FragmentViewDirWS(viewDir);
            #ifdef _SHADOWS_ENABLED
                inputData.shadowCoord = IN.shadowCoord;
            #else
                inputData.shadowCoord = float4(0, 0, 0, 0);
            #endif
                inputData.fogCoord = IN.fogFactorAndVertexLight.x;
                inputData.vertexLighting = IN.fogFactorAndVertexLight.yzw;
                inputData.bakedGI = SAMPLE_GI(IN.lightmapUV, IN.vertexSH, inputData.normalWS);
            }
            
            half4 sampleBackground(float2 texcoord){
                float y = tex2D(_textureY, texcoord).r;
                float4 ycbcr = float4(y, tex2D(_textureCbCr, texcoord).rg, 1.0);

				const float4x4 ycbcrToRGBTransform = float4x4(
						float4(1.0, +0.0000, +1.4020, -0.7010),
						float4(1.0, -0.3441, -0.7141, +0.5291),
						float4(1.0, +1.7720, +0.0000, -0.8860),
						float4(0.0, +0.0000, +0.0000, +1.0000)
					);

				return mul(ycbcrToRGBTransform, ycbcr);
            }
            
            half4 Frag(LightweightVertexOutput IN) : SV_Target
            {
                float4 baseColor = float4(0, 0, 0, 1);

                SurfaceData surfaceData;
                InitializeStandardLitSurfaceData(IN.uv, surfaceData);
                
                float freshnel = pow(1 -abs(dot(IN.viewDir, mul(UNITY_MATRIX_V, IN.normal))), 4) * 0.65;
                
                float ptrn1 = tex2D(_Pattern1, IN.uv * surfaceData.normalTS.xy * 0.3 + _Time.y * 0.25).r * 1.5;
                float ptrn2 = tex2D(_Pattern2, IN.uv * 0.5 + _Time.y * 0.1).r;
                
                float blend = ptrn1 * ptrn2;
                

                InputData inputData;
                InitializeInputData(IN, surfaceData.normalTS, inputData);
                
                float2 uvCoord = (IN.clipPosCopy.xy/IN.clipPosCopy.w + 1) * 0.5;
                
                uvCoord.x = 1.0 - uvCoord.x;
                
                half4 back = sampleBackground(uvCoord + surfaceData.normalTS.xy * 0.5);
   
                baseColor = SampleAlbedoAlpha(IN.uv, TEXTURE2D_PARAM(_MainTex, sampler_MainTex));

                float4 litColor = LightweightFragmentPBR(inputData, baseColor.rgb, surfaceData.metallic, surfaceData.specular, surfaceData.smoothness, 1, surfaceData.emission, baseColor.a);
                
                float4 stealth = back * 0.8 + half4(_Color.r, _Color.g, _Color.b, 0.0) * max(blend + freshnel, 0.075);
                
                litColor = lerp(litColor, stealth, _StealthAmount);
                
                return litColor;
            }
            
            
            ENDHLSL
        }

    }
    FallBack "Hidden/InternalErrorShader"
    CustomEditor "LightweightStandardGUI"
}
