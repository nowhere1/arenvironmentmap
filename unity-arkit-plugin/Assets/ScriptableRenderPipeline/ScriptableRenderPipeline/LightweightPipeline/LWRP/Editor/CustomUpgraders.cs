﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.Rendering;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace ScriptableRenderPipeline.ScriptableRenderPipeline.LightweightPipeline.LWRP.Editor
{
    public class CustomMaterialUpgrader
    {
        [MenuItem("Edit/Custom Pipeline/Upgrade Project Materials to LightWeight Materials", priority =
            CoreUtils.editMenuPriority2)]

        private static void UpgradeProjectMaterials()
        {
            if (!EditorUtility.DisplayDialog(DialogText.title, "The upgrade will overwrite materials in your project. " + DialogText.projectBackMessage, DialogText.proceed, DialogText.cancel))
                return;

            ReplaceShaders("Standard (Roughness setup)", "LightweightPipeline/Standard (Physically Based)");
        }

        [MenuItem("Edit/Custom Pipeline/Restore Project Materials to Standart Materials", priority =
            CoreUtils.editMenuPriority2)]

        private static void RestoreProjectMaterials()
        {
            if (!EditorUtility.DisplayDialog(DialogText.title, "The restore will overwrite materials in your project. " + DialogText.projectBackMessage, DialogText.proceed, DialogText.cancel))
                return;

            ReplaceShaders("LightweightPipeline/Standard (Physically Based)", "Standard (Roughness setup)");
        }

        public static void ReplaceShaders(string from, string to)
        {
            List<string> mats = UnityEditor.AssetDatabase.GetAllAssetPaths().Where(IsMaterialPath).ToList();
            int totalMaterialCount = mats.Count;

            int materialIndex = 0;
            foreach (string path in mats)
            {
                materialIndex++;
                if (UnityEditor.EditorUtility.DisplayCancelableProgressBar("Upgrading", string.Format("({0} of {1}) {2}", materialIndex, totalMaterialCount, path), (float)materialIndex / (float)totalMaterialCount))
                    break;

                Material m = UnityEditor.AssetDatabase.LoadMainAssetAtPath(path) as Material;

                if (m != null)
                {
                    if (m.shader.name == from)
                    {
                        Upgrade(m, to);
                    }
                }
            }

            UnityEditor.EditorUtility.ClearProgressBar();
        }

        public static void Upgrade(Material material, string newShader)
        {
            Material newMaterial;
            newMaterial = UnityEngine.Object.Instantiate(material) as Material;
            newMaterial.shader = Shader.Find(newShader);

            material.shader = Shader.Find(newShader);
            material.CopyPropertiesFromMaterial(newMaterial);
            UnityEngine.Object.DestroyImmediate(newMaterial);
        }

        static bool IsMaterialPath(string path)
        {
            return path.EndsWith(".mat", StringComparison.OrdinalIgnoreCase);
        }
    }
}
