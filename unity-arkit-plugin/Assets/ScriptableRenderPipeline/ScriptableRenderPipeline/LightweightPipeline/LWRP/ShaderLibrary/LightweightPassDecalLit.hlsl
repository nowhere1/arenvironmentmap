#ifndef LIGHTWEIGHT_PASS_LIT_INCLUDED
#define LIGHTWEIGHT_PASS_LIT_INCLUDED

#include "LWRP/ShaderLibrary/Lighting.hlsl"

struct LightweightVertexInput
{
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float4 tangent : TANGENT;
    float2 texcoord : TEXCOORD0;
    float2 lightmapUV : TEXCOORD1;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct LightweightVertexOutput
{
    float2 uv                       : TEXCOORD0;
    DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);
    float3 posWS                    : TEXCOORD2;

#ifdef _NORMALMAP
    half4 normal                    : TEXCOORD3;    // xyz: normal, w: viewDir.x
    half4 tangent                   : TEXCOORD4;    // xyz: tangent, w: viewDir.y
    half4 binormal                  : TEXCOORD5;    // xyz: binormal, w: viewDir.z
    half3 viewDir                   : TEXCOORD8;
#else
    half3  normal                   : TEXCOORD3;
    half3 viewDir                   : TEXCOORD4;
#endif

    half4 fogFactorAndVertexLight   : TEXCOORD6; // x: fogFactor, yzw: vertex light

#ifdef _SHADOWS_ENABLED
    float4 shadowCoord              : TEXCOORD7;
#endif

    float4 clipPos                  : SV_POSITION;
    float4 clipPosCopy              : COLOR0;
    UNITY_VERTEX_INPUT_INSTANCE_ID
    UNITY_VERTEX_OUTPUT_STEREO
};

void InitializeInputData(LightweightVertexOutput IN, float4 normal, float3 pos, out InputData inputData)
{
    inputData = (InputData)0;

    inputData.positionWS = pos;

#ifdef _NORMALMAP
    half3 viewDir = half3(normal.w, IN.tangent.w, IN.binormal.w);
    inputData.normalWS = TangentToWorldNormal(normal.xyz, IN.tangent.xyz, IN.binormal.xyz, normal.xyz);
#else
    half3 viewDir = VertexViewDirWS(GetCameraPositionWS() - pos);
    inputData.normalWS = FragmentNormalWS(normal);
#endif

    inputData.viewDirectionWS = FragmentViewDirWS(viewDir);
    inputData.shadowCoord = float4(0, 0, 0, 0);

    inputData.fogCoord = IN.fogFactorAndVertexLight.x;
    inputData.vertexLighting = IN.fogFactorAndVertexLight.yzw;
    inputData.bakedGI = SAMPLE_GI(IN.lightmapUV, IN.vertexSH, normal);
}

///////////////////////////////////////////////////////////////////////////////
//                  Vertex and Fragment functions                            //
///////////////////////////////////////////////////////////////////////////////

// Used in Standard (Physically Based) shader
LightweightVertexOutput LitPassVertex(LightweightVertexInput v)
{
    LightweightVertexOutput o = (LightweightVertexOutput)0;
    o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);

    float3 posWS = TransformObjectToWorld(v.vertex.xyz);
    o.clipPos = TransformWorldToHClip(posWS);
    o.clipPosCopy = o.clipPos;

    half3 viewDir = VertexViewDirWS(GetCameraPositionWS() - posWS);

#ifdef _NORMALMAP
    o.normal.w = viewDir.x;
    o.tangent.w = viewDir.y;
    o.binormal.w = viewDir.z;
#endif

    o.viewDir = normalize(mul(UNITY_MATRIX_V, float4(posWS, 1.0)));

    // initializes o.normal and if _NORMALMAP also o.tangent and o.binormal
    OUTPUT_NORMAL(v, o);
    OUTPUT_SH(o.normal.xyz, o.vertexSH);


    o.posWS = posWS;

    return o;
}

// Used in Standard (Physically Based) shader
half4 LitPassFragment(LightweightVertexOutput IN) : SV_Target
{
    UNITY_SETUP_INSTANCE_ID(IN);

    SurfaceData surfaceData;
    InitializeStandardLitSurfaceData(IN.uv, surfaceData);

    InputData inputData;
    //InitializeInputData(IN, surfaceData.normalTS, , inputData);

    half4 color = LightweightFragmentPBR(inputData, surfaceData.albedo, surfaceData.metallic, surfaceData.specular, surfaceData.smoothness, surfaceData.occlusion, surfaceData.emission, surfaceData.alpha);
    
    return color;
}

#endif
