﻿using UnityEngine;
using UnityEngine.Rendering;

namespace PortalHunter.Graphics
{
	public class CameraMaster : MonoBehaviour
	{
		public bool enabled = true;

		public bool blitEnabled = false;
		
		public Camera mainCamera, additiveCamera, blendCamera;
		public Material blendMaterial;

		public Material backMaterial;

		private RenderTexture tempOpaque, tempAdditive;
		public  RenderTexture editorOpaque, editorAdditive, Depth;

		public Transform shadowPlane;

		public static CameraMaster inst;

		public float resolutionMultiplier = 0.75f;

		public int width, height;


		public float floorY = -1.5f;
		

		public void AdjustRenderTextureSizes()
		{
			inst = this;

			width = (int) (Screen.width * resolutionMultiplier);
			height = (int) (Screen.height * resolutionMultiplier);
			
			mainCamera.targetTexture = new RenderTexture(width, height, 32, RenderTextureFormat.ARGB32);
			
//			RenderTextureDescriptor desc = new RenderTextureDescriptor
//			{
//				width = width,
//				height = height,
//				msaaSamples = 4,
//				colorFormat = RenderTextureFormat.ARGB32,
//				depthBufferBits = 32,
//				volumeDepth = 1,
//				dimension = TextureDimension.Tex2D,
//				useMipMap = false
//			};
//
//			mainCamera.targetTexture = new RenderTexture(desc);
//			
//			RenderTextureDescriptor descDepth = new RenderTextureDescriptor
//			{
//				width = width,
//				height = height,
//				msaaSamples = 4,
//				colorFormat = RenderTextureFormat.Depth,
//				depthBufferBits = 0,
//				volumeDepth = 1,
//				dimension = TextureDimension.Tex2D,
//				useMipMap = false
//			};
			
			Depth = new RenderTexture(width, height, 32, RenderTextureFormat.Depth);
			Depth.Create();
			
			additiveCamera.targetTexture = new RenderTexture(width/2, height/2, 32, RenderTextureFormat.ARGB32);

			blendMaterial.SetTexture("_MainTex", mainCamera.targetTexture);
			blendMaterial.SetTexture("_AdditiveTex", additiveCamera.targetTexture);
		}

	}
}
