﻿Shader "Unlit/BlendShader"
{
	Properties
	{
		_MainTex ("Color", 2D) = "white" {}
		_BloodTexture ("Blood", 2D) = "white" {}

		_AdditiveTex ("Additive", 2D) = "black" {}
		
		_VignetteTint("Color", Color) = (0, 0, 0, 0)
		
		_intensity("Intensity", Range (0, 1)) = 1.0
		
		_StealthAmount("Stealth", Range(0, 1))  = 0.0
		_StealthTint("Color", Color) = (0, 0.07, 0.2, 1.0)
		
		_MaxRadius("_MaxRadius", Range (0, 4)) = 1.5
		_MinRadius("_MinRadius", Range (0, 4)) = 2
		
		_BW("_BW", Range (0, 1)) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;

			sampler2D _AdditiveTex;
			sampler2D _BloodTexture;
			
			float4 _MainTex_ST;
			
			fixed4 _VignetteTint;
			fixed4 _StealthTint;
			
			float _intensity;
			float _MinRadius;
			float _MaxRadius;
			
			float _StealthAmount;
			
			float _BW;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				
				fixed4 additive = tex2D(_AdditiveTex, i.uv);
				//i.uv.y = 1-i.uv.y;
				
				fixed4 opaque = tex2D(_MainTex, i.uv);
				fixed4 blended = opaque + additive;
				
				float damageVignette = pow(length(i.uv - 0.5) * _MinRadius, _MaxRadius);
				float stealthVignette = pow(length(i.uv - 0.5) * 1.3, 2);
				
				//stealth
				blended = blended *  lerp(fixed4(1, 1, 1, 1), _StealthTint, stealthVignette * _StealthAmount);
				//damage
				blended = blended *  lerp(fixed4(1, 1, 1, 1), _VignetteTint, damageVignette * _intensity);
		
				float intensity = dot(blended, fixed4(0.3, 0.59, 0.11, 0));
				
				blended = lerp(blended, fixed4(intensity, intensity, intensity, intensity), _BW);
				
				blended.a = 1;

				return blended;
			}
			ENDCG
		}
	}
}
